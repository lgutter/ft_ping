# ft_ping
This project aims to code a version of the popular utility "ping" from scratch. It is part of the Codam Advanced curriculum.

## allowed functions
Besides a version of printf we built as part of the Codam Core curriculum, and a collection of libc functions we had to create for the project "libft", only the following list of functions is allowed:
* getpid
* getuid
* getaddrinfo
* gettimeofday
* inet_ntop
* inet_pton
* exit
* signal
* alarm
* setsockopt
* recvmsg
* sendto
* socket


